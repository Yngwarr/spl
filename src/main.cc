#include <string_view>
#include <iostream>
#include <variant>
#include <vector>

struct Atom {
    std::string_view value;
};

using Node = std::variant<Atom, std::vector<Atom>>;

Node read_node(std::string str) {
    for (auto& ch : str) {
        std::cout << ch << std::endl;
    }
}

int main(int argc, char** argv) {
    //if (argc != 2) {
        //std::cerr << "Usage: spl [file]" << std::endl;
        //return 64;
    //}
    read_node("(+ 10 2)");
    return 0;
}
